/**
 * Created by Misikir on 1/22/2017.
 */
var express = require('express');
var opn = require('opn');
var path = require('path');
var http = require('http');

var app = express();
var port = 11111;

app.use(express.static(path.join(__dirname, '/')));
app.use('/dist', express.static(path.join(__dirname, '../dist')));
app.use('/*', express.static(path.join(__dirname, '/index.html')));
app.listen(port, function () {
  opn('http://localhost:' + port);
  console.log('------------------- CCMS Back Office -------------------- ');
  console.log('CCMS Back Office is running on port http://localhost:' + port + ' !');
  console.log('---------------- Server is running ----------------- ');

});


