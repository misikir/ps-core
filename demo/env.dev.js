/**
 * Created by Misikir on 11/25/2016.
 */
;(function (window) {
  'use strict';

  window.__env = window.__env || {};

  // API url
  window.__env.apiUrl = 'http://192.168.1.7:8083/api/';
  window.__env.authUrl = 'http://192.168.1.7:8083/api/';

  // Base url
  window.__env.baseUrl = '/';

  // Whether or not to enable debug mode
  // Setting this to false will disable console output
  window.__env.enableDebug = true;
}(this));
