/**
 * Created by Misikir on 4/26/2017.
 */
(function () {
  'use strict';

  angular
    .module('sampleModule')
    .config(config);

  config.$inject = ['$stateProvider'];

  /* @ngInject */
  function config($stateProvider) {
    $stateProvider
      .state('sample', {
        url: "/sample",
        templateUrl: "app/sample/views/index.html",
        controller: 'IndexSampleController as indexCtrl',
        data: {}
      })
      .state('account', {
        url: "/account",
        templateUrl: "app/sample/views/account.html",
        controller: 'AccountSampleController as accountCtrl',
        data: {}
      })
      .state('account.register', {
        url: "/register",
        templateUrl: "app/service-discovery/views/home.html",
        controller: 'RegisterAccountController as registerCtrl',
        data: {}
      })

  }
})();
