/**
 * Created by Misikir on 4/26/2017.
 */
(function () {
  'use strict';

  angular
    .module('sampleModule')
    .controller('IndexSampleController', IndexSampleController);

  IndexSampleController.$inject = ['$log'];

  /* @ngInject */
  function IndexSampleController($log) {
    var vm = this;
    vm.title = 'IndexSampleController';

    activate();

    ////////////////

    function activate() {
      $log.log(vm.title);
    }
  }

})();

