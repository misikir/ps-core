/**
 * Created by Misikir on 4/26/2017.
 */
(function () {
  'use strict';

  angular
    .module('sampleModule')
    .controller('AccountSampleController', AccountSampleController);

  AccountSampleController.$inject = ['$log', 'api','psAuthentication'];

  /* @ngInject */
  function AccountSampleController($log,api,psAuthentication) {
    var vm = this;
    vm.title = 'AccountSampleController';

    activate();

    ////////////////

    function activate() {
      $log.log(vm.title);
      api.post('news', {data: 'data'}).then(function (data) {
          $log.log(data)
        }, function (error) {
          $log.log(error);
        }
      );
      var loginData={
        username:'misikir',
        password:'adane'
      }
      psAuthentication.login(loginData).then(function (data) {
          $log.log(data)
        }, function (error) {
          $log.log(error);
        }
      )
    }
  }

})();

