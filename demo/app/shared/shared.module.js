/**
 * Created by Misikir on 12/3/2016.
 */

(function () {
  'use strict';
  var env = {};

  // Import variables if present (from env.js)
  if (window) {
    Object.assign(env, window.__env);
  }

  angular
    .module('sharedModule', [])
    .constant('__env', env);

})();
