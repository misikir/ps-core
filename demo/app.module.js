;(function () {
  'use strict';

  /**
   * @ngdoc overview
   * @name eServicePortal
   * @description
   * # eServicePortal
   *
   * Main module of the application.
   */

  var env = {};

  // Import variables if present (from env.js)
  if (window) {
    Object.assign(env, window.__env);
  }

  angular
    .module('psCoreTest', [
      'ps.angular',
      'ps.core',

      /*--- Must have module ---*/
      'sharedModule',


      /*--- Business modules ---*/
      'sampleModule'
    ])
    .constant('__env', env);
})();
