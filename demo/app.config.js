/**
 * Created by Misikir on 9/6/2016.
 */

(function () {
  'use strict';
  angular.module('psCoreTest')
    .config(psCoreConfig);

  psCoreConfig.$inject = [
    '__env',
    '$httpProvider',
    '$translateProvider',
    'psMessagingProvider',
    'psAuthenticationProvider',
    'psAuthorizationProvider',
    'psTranslationProvider'
  ];

  /* @ngInject */
  function psCoreConfig(__env,
                        $httpProvider,
                        $translateProvider,
                        psMessagingProvider,
                        psAuthenticationProvider,
                        psAuthorizationProvider,
                        psTranslationProvider) {

    psAuthenticationProvider.config({
      endpoint: {
        login: __env.authUrl + "OAuth2/Token",
        logout: __env.authUrl + "logout"
      }
    });
    psAuthorizationProvider.config({
      endpoint: {
        getAllRoles: __env.apiUrl + "Role/GetAllRoles",
        getPermission: __env.apiUrl + "",
        setPermissions: __env.apiUrl + "",
        hasPermission: __env.apiUrl + ""
      }
    });

    $translateProvider.useStaticFilesLoader({
      prefix: 'assets/translations/validation/',
      suffix: '.json'
    }, {
      prefix: 'assets/translations/',
      suffix: '.json'
    });

    // define translation maps you want to use on startup
    $translateProvider.preferredLanguage('en');
    $translateProvider.useSanitizeValueStrategy('sanitize');


    psMessagingProvider.setDefaultMessaging({
      iconSource: 'fontAwesome',
    });
    psMessagingProvider.setDefaultNotify({
      sound: false,
    });
    psTranslationProvider.config({
      endpoint: {
        getTranslation: __env.apiUrl + "Translation/getTranslation",
        getSuggestion: __env.apiUrl + "Translation/GetSuggestion",

        getTranslationByModule: __env.apiUrl + "Translation/getTranslationByModule",
        getTranslationByKey: __env.apiUrl + "Translation/getTranslationByKey",
        getTranslationByKeyByModule: __env.apiUrl + "Translation/getTranslationByKeyByModule",

        saveTranslationByModule: __env.apiUrl + "Translation/SaveTranslationByModule",
      }
    });

  }

})();
(function () {
  'use strict';
  angular.module('psCoreTest')
    .config(logger);

  logger.$inject = ['$provide', '__env'];

  /* @ngInject */
  function logger($provide, __env) {
    $provide.decorator('$log', function ($delegate) {
      var originalFns = {};

      // Store the original log functions
      angular.forEach($delegate, function (originalFunction, functionName) {
        originalFns[functionName] = originalFunction;
      });

      var functionsToDecorate = ['log', 'info', 'debug', 'warn', 'error'];

      // Apply the decorations
      angular.forEach(functionsToDecorate, function (functionName) {
        $delegate[functionName] = logDecorator(originalFns[functionName]);
      });

      return $delegate;
    });

    function logDecorator(fn) {
      return function () {
        if (!__env.enableDebug) {
          return false;
        }


        var args = [].slice.call(arguments);

        // Insert a separator between the existing log message(s) and what we're adding.
        args.push(' - ');

        // Use (instance of Error)'s stack to get the current line.
        var newErr = new Error();

        // phantomjs does not support Error.stack and falls over so we will skip it
        if (typeof newErr.stack !== 'undefined') {
          var stack = newErr.stack.split('\n').slice(1);

          if (navigator.userAgent.indexOf("Chrome") > -1) {
            stack.shift();
          }
          stack = stack.slice(0, 1);

          var stackInString = stack + '';
          var splitStack;
          if (navigator.userAgent.indexOf("Chrome") > -1) {
            splitStack = stackInString.split(" ");
          } else {
            splitStack = stackInString.split("@");
          }
          var lineLocation = splitStack[splitStack.length - 1];
          // Put it on the args stack.
          args.push(lineLocation);

          // Call the original function with the new args.
          fn.apply(fn, args);
        }
      };
    }
  }

})();

