/**
 * Created by Misikir on 11/25/2016.
 */
;(function () {
  'use strict';
  angular.module('psCoreTest')
    .config(route);

  route.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

  /* @ngInject */
  function route($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise("/");
    $locationProvider.html5Mode(true);

    $stateProvider
      .state('404', {
        url: '/404',
        templateUrl: '404.html'

      })

  }

})();
