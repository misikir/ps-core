/**
 * Created by misikirA on 4/20/2017.
 */
(function () {
  'use strict';

  angular.module('ps.core')
    .run(['$rootScope', '$state', function ($rootScope, $state) {

      $rootScope.$on('$stateChangeStart', function (evt, to, params) {
        if (to.redirectTo) {
          evt.preventDefault();
          $state.go(to.redirectTo, params, {location: 'replace'})
        }
      });
    }]);

})();

(function () {
  'use strict';

  angular
    .module('ps.core')
    .run(run);
  run.$inject = ['$log', '$rootScope', '$state', '__env', 'gettextCatalog', 'psTranslation'];

  /* @ngInject */
  function run($log, $rootScope, $state, __env, gettextCatalog, psTranslation) {
    gettextCatalog.debug = __env.enableDebug;
    loadLanguage();

    $rootScope.$on('$stateChangeStart', function (evt, to, params) {
      loadLanguage()
    });

    function loadLanguage() {
      psTranslation.getCurrentLanguage().then(
        function (data) {
          $log.log('current Language:', data.code);
          gettextCatalog.setCurrentLanguage(data.code);

        }
      );
    }
  }

})();
