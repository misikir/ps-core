/**
 * Created by misikirA on 4/14/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core', [
      /*------------------*/
      /*---  Views   ---*/
      /*------------------*/
      'ps.core.views',

      /*------------------*/
      /*---  Modules   ---*/
      /*------------------*/
      'ps.core.translation',
      'ps.core.account',
      /*------------------*/
      /*--- Directive  ---*/
      /*------------------*/
      'ps.core.collection',
      'ps.core.wizard',
      'ps.core.nav-bar',
      'ps.core.page-title',
      /*------------------*/
      /*---- Service  ----*/
      /*------------------*/
      'ps.core.api',
      'ps.core.storage',
      'ps.core.messaging',


      /*------------------*/
      /*---- Filter   ----*/
      /*------------------*/
      'ps.core.utility'
    ])
})();
