/**
 * Created by misikirA on 2/1/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.collection')
    .directive('collection', collection);

  collection.$inject = ['$log'];

  /* @ngInject */
  function collection($log) {
    var directive = {
      transclude: true,
      scope: {
        colHeader: '@',
        colHeight: '@',
        colInput: '=',
        colInputTitle: '@',
        colInputActive: '@',
        colInputTemplate: '@',
        colOutput: '=',
        colOutputTitle: '@',
        colOutputActive: '@',
        colOutputTemplate: '@'
      },
      replace: true,
      restrict: 'EA',
      templateUrl: 'app/ps-core/modules/collection/views/collection.html',
      controller: ['$scope', '$element', function CollectionController($scope, $element) {

        $scope.inputData = $scope.colInput;
        $scope.outputData = $scope.colOutput;
        $scope.header = $scope.colHeader;
        $scope.count = 0;
        $scope.add = add;
        $scope.detail = detail;
        $scope.remove = removeCol;
        $scope.getOutput = getOutput;
        var val;

        activate();

        function activate() {

          if ($scope.colHeight === undefined) {
            $scope.colHeight = 400;
          }


          if ($scope.colInputActive === undefined) {

            $scope.active = true;
          } else {
            val = ($scope.colInputActive === "true");

            $scope.inputActive = val;
          }
          if ($scope.colOutputActive === undefined) {

            $scope.active = true;
          } else {
            val = ($scope.colOutputActive === "true");

            $scope.outputActive = val;
          }


          $element.css('height', $scope.colHeight + 'px');
          $element.find('.clctn-lft').css('height', $scope.colHeight + 'px');
          $element.find('.clctn-rght').css('height', $scope.colHeight + 'px');


          $scope.inputData = _.map($scope.inputData, function (element) {
            var output = _.find($scope.outputData, element);

            if (output) {
              var status = {npColSelected: true, npColActive: false}
            } else {
              var status = {npColSelected: false, npColActive: false}
            }
            return _.extend({}, element, status);
          });

          if ($scope.header === undefined) {
            $scope.header = 'name';
          }

        }

        function add(col) {
          col.npColSelected = true;
          getOutput();
          count();
        }

        function detail(col) {
          col.npColActive = !col.npColActive;

        }

        function removeCol(col) {
          col.npColSelected = false;
          getOutput();
          count();
        }


        function getOutput() {
          $scope.colOutput = _.filter($scope.inputData, {npColSelected: true});
        }

        function count() {
          $scope.count = _.filter($scope.inputData, {npColSelected: true}).length;
        }


      }]
    };
    return directive;
  }

})();

