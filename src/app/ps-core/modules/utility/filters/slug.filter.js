/**
 * Created by misikirA on 3/24/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.utility')
    .filter('slug', slug);

  function slug() {
    return slugFilter;

    ////////////////

    function slugFilter(input) {
      if (!input) {
        return '';
      }
      return input.toLowerCase().replace(/ /g, "_");
    }
  }

})();
