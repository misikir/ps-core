/**
 * Created by Misikir on 11/24/2016.
 */
(function () {
  'use strict';


  /*
   * Creates a range
   * Usage example: <option ng-repeat="y in [] | range:1998:1900">{{y}}</option>
   */


  angular
    .module('ps.core.utility')
    .filter('range', range);

  function range() {
    return rangeFilter;

    ////////////////

    function rangeFilter(input, start, end) {
      var direction;

      start = parseInt(start);
      end = parseInt(end);

      if (start === end) { return [start]; }

      direction = (start <= end) ? 1 : -1;

      while (start != end) {
        input.push(start);

        if (direction < 0 && start === end + 1) {
          input.push(end);
        }

        if (direction > 0 && start === end - 1) {
          input.push(end);
        }

        start += direction;
      }

      return input;
    }
  }

})();

