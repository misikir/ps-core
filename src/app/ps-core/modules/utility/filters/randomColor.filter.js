/**
 * Created by Misikir on 11/29/2016.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.utility')
    .filter('randomColor', randomColor);

  function randomColor() {
    return randomColorFilter;

    ////////////////

    function randomColorFilter() {
      var clr = [
        '#FFFB00',
        '#84CF00',
        '#8C20BD',
        '#00B2DE',
        '#FF2000',
        '#FF7D00',
        '#0024BD',
        '#00BA00',
        '#FFCF00',
        '#F72494',
        '#0079C6',
        '#FFA600'];
      return clr[Math.floor(Math.random() * clr.length)];
    }
  }

})();

