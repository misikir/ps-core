/**
 * Created by Misikir on 2/9/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.utility')
    .filter('cameCaseToHuman', cameCaseToHuman);

  function cameCaseToHuman() {
    return cameCaseToHumanFilter;

    ////////////////

    function cameCaseToHumanFilter(input, upperCaseFirst) {

      if (typeof input !== "string") {
        return input;
      }

      var result = input.replace(/([a-z])([A-Z])/g, '$1 $2')
        .replace(/([A-Z])([a-z])/g, ' $1$2')
        .replace(/\ +/g, ' ');

      if (upperCaseFirst) {
        result = result.charAt(0).toUpperCase() + result.slice(1);
      }

      return result;
    }
  }

})();

