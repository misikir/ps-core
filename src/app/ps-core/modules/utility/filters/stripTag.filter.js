/**
 * Created by misikirA on 4/13/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.utility')
    .filter('stripTag', stripTag);

  function stripTag() {
    return stripTagFilter;

    ////////////////

    function stripTagFilter(input) {

     // input = "..." + input;
     // return input.replace(/(<([^>]+)>)/g, "");
      return  input ? String(input).replace(/<[^>]+>/gm, '') : '';
      //return angular.element(String(input)).text();

    }


  }

})();
