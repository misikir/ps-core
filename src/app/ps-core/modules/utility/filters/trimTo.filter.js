/**
 * Created by Misikir on 2/14/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.utility')
    .filter('trimTo', trimTo);

  function trimTo() {
    return trimToFilter;

    ////////////////

    function trimToFilter(input, limit, ellipsis) {
      if (!input) {
        return '';
      }
      var extra = " ...";
      if (ellipsis) {
        extra = ellipsis
      }

      var result = input.substr(0, limit);
      if (input.length > limit) {
        result = result + ' ' + extra;
      }
      return result
    }
  }

})();

