/**
 * Created by Misikir on 11/28/2016.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.utility')
    .filter('capitalize', capitalize);

  function capitalize() {
    return capitalizeFilter;

    ////////////////

    function capitalizeFilter(input, all) {
      if (!input) {
        return '';
      }
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }) : '';
    }
  }

})();

