/**
 * Created by Misikir on 3/28/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.translation')
    .provider('psTranslation', psTranslation);


  psTranslation.$inject = [];

  /* @ngInject */
  function psTranslation() {
    var _options = {
      endpoint: {
        getTranslation: '',
        getSuggestion: '',

        getTranslationByModule: '',
        getTranslationByKey: '',
        getTranslationByKeyByModule: '',

        saveTranslationByModule: ''
      }
    };

    /* jshint validthis:true */
    this.$get = translationHelper;
    this.config = config;

    translationHelper.$inject = ['$log', '$q', 'api', 'psStorage'];

    function config(options) {
      _options = angular.merge({}, _options, options);
    }

    /* @ngInject */
    function translationHelper($log, $q, api, psStorage) {

      var service = {
        getLanguages: getLanguages,
        getActiveLanguages: getActiveLanguages,
        getTranslation: getTranslation,
        getCurrentLanguage: getCurrentLanguage,
        setCurrentLanguage: setCurrentLanguage,
        getSuggestion: getSuggestion,
        saveTranslationByModule: saveTranslationByModule
      };
      return service;

      ////////////////

      function getLanguages() {
        return [
          {code: "en", name: "English"},
          {code: "am", name: "አማርኛ"},
          {code: "om", name: "Afaan Oromo"},
          {code: "ti", name: "ትግርኛ"}
        ]
      }

      function getActiveLanguages() {
        return [
          {code: "am", name: "አማርኛ"},
          {code: "om", name: "Afaan Oromo"},
          {code: "ti", name: "ትግርኛ"}
        ]
      }

      function getCurrentLanguage() {
        var deferred = $q.defer();
        psStorage.getObject('language').then(
          function (data) {
            deferred.resolve(JSON.parse(data));
          });
        return deferred.promise;
      }

      function setCurrentLanguage(data) {
        var deferred = $q.defer();
        psStorage.setObject('language', data).then(
          function (d) {
            deferred.resolve(d);
          });
        return deferred.promise;
      }

      function getSuggestion(data) {
        return base(_options.endpoint.getSuggestion, data);
      }

      function saveTranslationByModule(data) {
        return base(_options.endpoint.saveTranslationByModule, data);
      }

      function getTranslation(data) {
        var deferred = $q.defer();
        api.post(_options.endpoint.getTranslation, data).then(
          function (d) {
            psStorage.setObject('translation', d.data).then(
              function () {
                deferred.resolve(d.data);
              });

          },
          function (err) {
            $log.log(err);
            return $q.reject(err)
          });

        return deferred.promise;
      }

      /*--------   Base   -----------*/

      function base(url, data) {
        var deferred = $q.defer();
        api.post(url, data).then(
          function (d) {
            deferred.resolve(d.data);
          },
          function (err) {
            $log.log(err);
            return $q.reject(err)
          });

        return deferred.promise;
      }
    }
  }

})();

