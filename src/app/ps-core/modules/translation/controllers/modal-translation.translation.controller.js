/**
 * Created by Misikir on 3/27/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.translation')
    .controller('ModalTranslationTranslationController', ModalTranslationTranslationController);

  ModalTranslationTranslationController.$inject = ['$log', '$uibModalInstance', 'translationService', 'messaging', 'translationMessage', 'toTranslate', 'inputType', 'moduleType'];

  /* @ngInject */
  function ModalTranslationTranslationController($log, $uibModalInstance, translationService, messaging, translationMessage, toTranslate, inputType, moduleType) {
    var vm = this;
    vm.title = 'ModalTranslationTranslationController';
    vm.toTranslate = toTranslate;
    vm.inputType = inputType;
    vm.moduleType = moduleType;
    vm.done = done;
    vm.cancel = cancel;
    activate();

    ////////////////

    function activate() {
      $log.log(vm.title);
      vm.languages = translationService.getActiveLanguages();

      vm.translation = {
        Key: vm.toTranslate,
        Module: vm.moduleType,
        Languages: []
      };


      _.map(vm.languages, function (o) {
        var language = {
          Code: o.code,
          Name: o.name,
          Value: {
            Translated: ''
          }
        };
        vm.translation.Languages.push(language)
      })


    }

    function done() {

      translationService.saveTranslationByModule(vm.translation).then(
        function (data) {
          showMessaging(data);
          $uibModalInstance.close(vm.translation);
        }, function (error) {
          $log.log('error', error);
          messaging.notification(translationMessage.I0001);
        });

    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');

    }

    function showMessaging(data) {
      if (!data.IsFailed) {
        messaging.notification(messaging.formatMessage(data.SystemMessage));

      } else {
        messaging.dialog(messaging.formatMessage(data.SystemMessage));
      }


      $log.log('data.SystemMessage:', messaging.formatMessage(data.SystemMessage));
    }
  }

})();

