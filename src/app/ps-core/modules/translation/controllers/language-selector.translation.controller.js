/**
 * Created by misikirA on 4/11/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.translation')
    .controller('LanguageSelectorTranslationController', LanguageSelectorTranslationController);

  LanguageSelectorTranslationController.$inject = ['$log', 'gettextCatalog', 'psTranslation'];

  /* @ngInject */
  function LanguageSelectorTranslationController($log, gettextCatalog, psTranslation) {
    var vm = this;
    vm.title = 'LanguageSelectorTranslationController';
    vm.selectedLanguage;
    vm.changeLanguage = changeLanguage;
    activate();

    ////////////////

    function activate() {
      $log.log(vm.title);

      vm.languages = psTranslation.getLanguages();
      vm.selectedLanguage = vm.languages[0].name;


    }

    function changeLanguage(selectedLanguage) {
      vm.selectedLanguage = selectedLanguage.name;

      psTranslation.getTranslation(selectedLanguage.code).then(
        function (data) {
          $log.log(data);
        });


      psTranslation.setCurrentLanguage(selectedLanguage).then(
        function (data) {
          var selectedLanguage = JSON.parse(data);
          gettextCatalog.setCurrentLanguage(selectedLanguage.code);
          $log.log('selectedLanguage', selectedLanguage);
        }
      );
      $log.log(gettextCatalog.getCurrentLanguage());
    }
  }

})();

