/**
 * Created by misikirA on 3/27/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.translation')
    .directive('psTranslate', psTranslate);

  psTranslate.$inject = ['$log', '$compile', '$timeout', '$uibModal'];

  /* @ngInject */
  function psTranslate($log, $compile, $timeout, $uibModal) {
    var directive = {
      link: link,
      restrict: 'A',
      //template: '',
      require: 'ngModel',
      scope: {
        ngModel: "=",
        psTranslateModule: "@",
        psTranslateInput: "@"
      }
    };
    return directive;

    function link(scope, element, attrs) {


      if (scope.psTranslateModule == undefined) {
        scope.psTranslateModule = "dictionary"
      }

      if ((scope.psTranslateInput == undefined) || (scope.psTranslateInput == "input")) {
        scope.inputType = "input"
      }
      else if (scope.psTranslateInput == "textarea") {
        scope.inputType = "textarea"
      }


      scope.openTranslator = function () {
        var modalInstance = $uibModal.open({
          size: 'md',
          templateUrl: 'app/ps-core/modules/collection/views/collection.html',
          controller: 'ModalTranslationTranslationController',
          controllerAs: 'translationCtrl',
          windowClass: 'validation-modal',
          windowTopClass: 'validation-modal',
          resolve: {
            toTranslate: function () {
              return scope.ngModel;
            },
            inputType: function () {
              return scope.inputType;
            },
            moduleType: function () {
              return scope.psTranslateModule;
            }
          }
        });

        modalInstance.result.then(
          function (selectedFormAttribute) {
            $log.log(selectedFormAttribute)
          }, function () {
            $log.log('Modal dismissed at: ' + new Date());
          });
      };

      scope.disable = false;
      if (scope.ngModel == undefined) {
        scope.disable = true;
      } else {
        scope.disable = false;
      }


      var translateBtn = '<div class="pstn-rltv">' +
        '<button class="btn  btn-default translate-btn" ' +
        'ng-disabled =" (ngModel == undefined) || (ngModel ==\'\' )" ' +
        'ng-click="openTranslator()">' +
        '<span class="badge bld">T</span>' +
        '</button>' +
        '</div>';

      var cc = $compile(translateBtn)(scope);
      var wrapper = angular.element(cc);

      element.after(wrapper);
      wrapper.prepend(element);


      scope.$on("$destroy", function () {
        wrapper.after(element);
        wrapper.remove();
      });


    }
  }


})();

