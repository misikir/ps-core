/**
 * Created by Misikir on 3/28/2017.
 */
(function () {
  'use strict';
  /***
   * Alert and Notification type
   * 'error E |success S |warning W |information I'
   *
   */
  angular
    .module('ps.core.translation')
    .constant('translationMessage',
      {
        /*------------ Error -------------------*/
        E0001: {
          type: "error",
          message: ""
        },
        /*------------ Success -------------------*/
        S0001: {
          type: "success",
          message: ""
        },
        /*------------ Warning -------------------*/
        W0001: {
          type: "warning",
          message: ""
        },
        /*------------ Info -------------------*/
        I0001: {
          type: "information",
          message: ""
        }

      }
    );
})();
