/**
 * Created by Misikir on 3/8/2017.
 */
(function () {
  'use strict';
  // Check we have lobibox js included
  if (angular.isUndefined(window.Lobibox)) {
    throw "Please make sure lobibox is included! https://github.com/arboshiki/lobibox";
  }
  angular.module('ps.core.messaging')
    .provider('psMessaging', function ProvideMessaging() {

      return ({
        setDefaultMessaging: setDefaultMessaging,
        setDefaultNotify: setDefaultNotify,
        $get: ['$log', 'api', instantiateMessaging]
      });

      function setDefaultMessaging(options) {

        Lobibox.base.DEFAULTS = $.extend({}, Lobibox.base.DEFAULTS, options);
      }

      function setDefaultNotify(options) {
        Lobibox.notify.DEFAULTS = $.extend({}, Lobibox.notify.DEFAULTS, options);

      }

      function instantiateMessaging($log, api) {

        return {
          dialog: dialog,
          notification: notification,
          formatMessage: formatMessage,
          /*------ Default ------------*/
          confirm: confirm,
          alert: alert,
          prompt: prompt,
          progress: progress,
          window: window,
          notify: notify

        };

        function formatMessage(systemMessage) {
          var system = {
            "Exception": null,
            "ExceptionMessage": null,
            "Culture": null,
            "MessageType": 0,
            "Type": "Error",
            "Message": "Attribute Name Already Exist Within Your Organization",
            "MessageCode": "E400",
            "Code": "E400-",
            "HasDetail": false,
            "ModuleCode": null
          };


          var sysMsg = {
            type: systemMessage.Type.toLowerCase(),
            message: systemMessage.Message
          };
          if (systemMessage.HasDetail) {
            sysMsg.buttons = {
              detail: {
                'class': 'btn btn-info',
                'name': 'Detail',
                closeOnClick: false
              }
            };
            sysMsg.callback = function (lobibox, type) {
              if (type === 'detail') {
                $log.log('Call extra Detail', systemMessage)
              }

            };

          }

          return sysMsg;
        }

        function dialog(message) {


          //message.msg="modified msg";
          message.msg = message.message;

          if (message.type == "information") {
            message.type = "info";
            message.title = "Information";
          } else if (message.type == "success") {
            message.title = "Success";
          } else if (message.type == "error") {
            message.title = "Error";
          } else if (message.type == "warning") {
            message.title = "Warning";
          } else {
            message.title = message.type;

          }

          Lobibox.alert(message.type, message);
        }

        function notification(message) {

          if (message.type == "information") {
            message.type = "info";
            message.title = "Information";
          } else if (message.type == "success") {
            message.title = "Success";
          } else if (message.type == "error") {
            message.title = "Error";
            message.delay = false;
          } else if (message.type == "warning") {
            message.title = "Error";
            message.delay = false;
          } else {
            message.title = message.type;
            message.delay = false;
          }


          message.msg = message.message;
          // message.position = {position: 'top left'} //AVAILABLE OPTIONS: 'top left', 'top right', 'bottom left', 'bottom right'
          Lobibox.notify(message.type, message);

        }


        function getMessage(code) {
          api.get().then(function (data) {

          })

        }

        /*------ Default ------------

         horizontalOffset: 5,                //If the messagebox is larger (in width) than window's width. The messagebox's width is reduced to window width - 2 * horizontalOffset
         verticalOffset: 5,                  //If the messagebox is larger (in height) than window's height. The messagebox's height is reduced to window height - 2 * verticalOffset
         width: 600,
         height: 'auto',                     // Height is automatically calculated by width
         closeButton: true,                  // Show close button or not
         draggable: false,                   // Make messagebox draggable
         customBtnClass: 'lobibox-btn lobibox-btn-default', // Class for custom buttons
         modal: true,
         debug: false,
         buttonsAlign: 'center',             // Position where buttons should be aligned
         closeOnEsc: true,                   // Close messagebox on Esc press
         delayToRemove: 200,                 // Time after which lobibox will be removed after remove call. (This option is for hide animation to finish)
         delay: false,                       // Time to remove lobibox after shown
         baseClass: 'animated-super-fast',   // Base class to add all messageboxes
         showClass: 'zoomIn',                // Show animation class
         hideClass: 'zoomOut',               // Hide animation class
         iconSource: 'bootstrap',            // "bootstrap" or "fontAwesome" the library which will be used for icons
         */
        function confirm(values) {
          Lobibox.confirm(values);
        }

        function alert(type, options) {
          Lobibox.alert(type, options);
        }

        function prompt(type, options) {
          Lobibox.prompt(type, options);
        }

        function progress(options) {
          Lobibox.progress(options);
        }

        function window(options) {
          Lobibox.window(options);
        }

        /*
         Default options for lobibox notifications
         -----------------------------------------
         title: true,                // Title of notification. Do not include it for default title or set custom string. Set this false to disable title
         size: 'normal',             // normal, mini, large
         soundPath: 'src/sounds/',   // The folder path where sounds are located
         soundExt: '.ogg',           // Default extension for all sounds
         showClass: 'zoomIn',        // Show animation class.
         hideClass: 'zoomOut',       // Hide animation class.
         icon: true,                 // Icon of notification. Leave as is for default icon or set custom string
         msg: '',                    // Message of notification
         img: null,                  // Image source string
         closable: true,             // Make notifications closable
         delay: 5000,                // Hide notification after this time (in miliseconds)
         delayIndicator: true,       // Show timer indicator
         closeOnClick: true,         // Close notifications by clicking on them
         width: 400,                 // Width of notification box
         sound: true,                // Sound of notification. Set this false to disable sound. Leave as is for default sound or set custom soud path
         position: "bottom right"    // Place to show notification. Available options: "top left", "top right", "bottom left", "bottom right"
         iconSource: "bootstrap"     // "bootstrap" or "fontAwesome" the library which will be used for icons
         rounded: false,             // Whether to make notification corners rounded
         messageHeight: 60,          // Notification message maximum height. This is not for notification itself, this is for .lobibox-notify-msg
         pauseDelayOnHover: true,    // When you mouse over on notification, delay will be paused, only if continueDelayOnInactiveTab is false.
         onClickUrl: null,           // The url which will be opened when notification is clicked
         showAfterPrevious: false,   // Set this to true if you want notification not to be shown until previous notification is closed. This is useful for notification queues
         continueDelayOnInactiveTab: true, // Continue delay when browser tab is inactive
         */
        function notify(type, options) {
          Lobibox.notify(type, options);
        }


      }

    });
})();
