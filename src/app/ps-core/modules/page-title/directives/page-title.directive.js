/**
 * Created by Misikir on 2/4/2017.
 */
/**
 * Created by Misikir on 10/3/2016.
 */
(function () {
  'use strict';

  /**
   * header title
   *
   * usage:
   * <page-title  name="CCMS" default="How can we help you?" ></page-title>
   *
   */

  angular.module('ps.core.page-title')
    .directive('pageTitle', pageTitle);

  pageTitle.$inject = ['$rootScope', '$timeout'];

  /* @ngInject */
  function pageTitle($rootScope, $timeout) {
    var directive = {
      restrict: 'EA',
      link: link,
      replace:true,
      scope: {
        name: "@",
        default: "@"
      }
    };

    return directive;

    function link(scope, element, attrs) {
      var title = scope.name + ' | ' + scope.default;
      element.replaceWith('<title>'+title+'</title>');

      var listener = function (event, toState, toParams, fromState, fromParams) {

        // Create your own title pattern
        if (toState.data && toState.data.pageTitle) title = scope.name + ' | ' + toState.data.pageTitle;
        $timeout(function () {
          element.replaceWith('<title>'+title+'</title>');

        });
      };
      $rootScope.$on('$stateChangeStart', listener);
    }
  }

})();

