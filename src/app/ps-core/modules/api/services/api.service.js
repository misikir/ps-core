/**
 * Created by Misikir on 11/19/2016.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.api')
    .provider('api', api);

  api.$inject = [];

  function api() {
    var _options = {
      endpoint: {}
    };

    /* jshint validthis:true */
    this.$get = apiHelper;
    this.config = config;

    apiHelper.$inject = ['$rootScope', '$log', '$q', '$http', '$timeout', '$state', 'Upload', '__env', 'psStorage'];

    /////////////////////////////

    function config(options) {
      _options = angular.merge({}, _options, options);
    }

    /* @ngInject */
    function apiHelper($rootScope, $log, $q, $http, $timeout, $state, Upload, __env, psStorage) {


      var service = {
        get: get,
        post: save,
        put: update,
        patch: patch,
        delete: remove,
        upload: upload,
        login: login,
      };
      return service;

      ////////////////


      function get(url) {
        var deferred = $q.defer();
        var req = {
          method: 'GET',
          url: _options.endpoint.hasOwnProperty(url) ? _options.endpoint[url] : url
        };

        base(req).then(function (data) {
          deferred.resolve(data);
        });
        return deferred.promise;
      }

      function save(url, data) {
        var deferred = $q.defer();
        var req = {
          method: 'POST',
          url: _options.endpoint.hasOwnProperty(url) ? _options.endpoint[url] : url,
          data: data
        };

        base(req).then(function (data) {
          deferred.resolve(data);
        });
        return deferred.promise;
      }

      function update(url, data) {


      }

      function patch() {

      }

      function remove() {

      }

      function upload(url, data) {
        var deferred = $q.defer();
        var u = _options.api.hasOwnProperty(url) ? _options.api[url] : url;

        var req = {
          url: u,
          data: data,
          objectKey: '',
          arrayKey: ''
        };

        psStorage.getObject('access_token').then(function (auth) {
          if (auth) {
            req = angular.merge({}, req, {headers: {'Authorization': 'Bearer ' + auth}});
          }
          $log.log(req);

          Upload.upload(req).then(function (resp) {
            deferred.resolve(resp);
          }, function (resp) {
            deferred.reject(resp);
          });

        });


        return deferred.promise;
      }

      function login(url, data) {
        var deferred = $q.defer();

        $http({
          method: 'POST',
          url: _options.endpoint.hasOwnProperty(url) ? _options.endpoint[url] : url,
          data: data,
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
          .then(
            function (response) {
              psStorage.setObject('access_token', response.data.access_token)
                .then(
                  function () {

                  });
              response.data.access_token = "NO Access Token";
              psStorage.setObject('authorizationData', response.data)
                .then(
                  function () {
                    deferred.resolve(response);
                  });
              deferred.resolve(response);
            },
            function (response) {

              deferred.reject(response);
            });
        return deferred.promise;
      };


      function base(req) {

        var deferred = $q.defer();


        psStorage.getObject('access_token')
          .then(
            function (auth) {
              if (auth) {
                req = angular.merge({}, req, {headers: {'Authorization': 'Bearer ' + auth}});
                $http(req).then(
                  function (data) {
                    deferred.resolve(data);
                  },
                  function (err) {
                    if (err.status === '401') {
                      $state.go('login');
                    }
                    return $q.reject(err)
                  });
              } else {
                $http(req).then(
                  function (data) {
                    deferred.resolve(data);
                  },
                  function (err) {
                    if (err.status === '401') {
                      $state.go('login');
                    }
                    return $q.reject(err)
                  });
              }
              // $log.log(req);


            },
            function (error) {
              $http(req).then(
                function (data) {
                  deferred.resolve(data);
                },
                function (err) {
                  if (err.status === '401') {
                    $state.go('login');
                  }
                  return $q.reject(err)
                });
              $log.log('access_token', error);
            });

        return deferred.promise;
      }
    }


  }
})();
