/**
 * Created by Misikir on 2/20/2017.
 */
;(function () {
  'use strict';

  angular.module('ps.core.account')
    .provider('psAuthorization', psAuthorization);

  psAuthorization.$inject = [];

  function psAuthorization() {
    var _options = {
      endpoint: {
        getAllRoles:'',
        getPermission: '',
        setPermissions: '',
        hasPermission: ''
      }
    };

    /* jshint validthis:true */
    this.$get = psAuthorizationHelper;
    this.config = config;

    psAuthorizationHelper.$inject = ['$rootScope', '$log', '$q', '$http', '$timeout', '$state', '__config', '$localForage'];
    function config(options) {
      _options = angular.merge({}, _options, options);
    }

    /* @ngInject */
    function psAuthorizationHelper($rootScope, $log, $q, $http, $timeout, $state, __config, $localForage) {
      var service = {
        getAllRoles:getAllRoles,
        getPermission: getPermission,
        setPermissions: setPermissions,
        hasPermission: hasPermission

      };
      return service;

      function getAllRoles() {

      }
      function getPermission() {

      }

      function setPermissions() {

      }

      function hasPermission() {

      }
    }


  }
})();
