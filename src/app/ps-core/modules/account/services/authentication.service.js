﻿/**
 * Created by Billet on 12/29/2016.
 */

;(function () {
  'use strict';

  angular.module('ps.core.account')
    .provider('psAuthentication', psAuthentication);

  psAuthentication.$inject = [];

  function psAuthentication() {
    var _options = {
      endpoint: {}
    };

    /* jshint validthis:true */
    this.$get = psAuthenticationHelper;
    this.config = config;

    psAuthenticationHelper.$inject = ['$rootScope', '$log', '$q', '$http', '$timeout', '$state', '$localForage'];
    function config(options) {
      _options = angular.merge({}, _options, options);
    }

    /* @ngInject */
    function psAuthenticationHelper($rootScope, $log, $q, $http, $timeout, $state, $localForage) {
      var service = {
        login: login,
        logout: logout,
        isLoggedIn: isLoggedIn
      };
      return service;

      function logout() {
        var deferred = $q.defer();
        $localForage.removeItem(['access_token', 'psAuthentication', 'permission']).then(function (data) {
          console.log(data);
          deferred.resolve(data);
        });
        return deferred.promise;
      }

      function login(loginData) {
        var deferred = $q.defer();


        var data = "grant_type=password&username=" + loginData.username + "&password=" + loginData.password + "&client_id=099153c2625149bc8ecb3e85e03f0022";

        $http({
          method: 'POST',
          url: _options.endpoint.login,
          data: data,
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(
          function (d) {
            $localForage.setItem('access_token', d.data.access_token).then(
              function (lfData) {
                d.data.access_token = "NO Access Token";
                $localForage.setItem('psAuthentication', d.data).then(
                  function (d) {
                    deferred.resolve(d);
                  });
              });

          },
          function (err) {
            deferred.reject(err.data);
          });
        return deferred.promise;
      };

      function isLoggedIn() {
        var deferred = $q.defer();
        $localForage.getItem('access_token').then(
          function (response) {
            deferred.resolve(response);
          });
        return deferred.promise;
      }
    }


  }
})();
