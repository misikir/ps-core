/**
 * Created by Misikir on 4/25/2017.
 */
(function () {
  'use strict';

  /**
   * header title
   *
   * usage:
   * <has-permission  name="CCMS" default="How can we help you?" ></has-permission>
   *
   */

  angular.module('ps.core.account')
    .directive('hasPermission', hasPermission);

  hasPermission.$inject = ['$log', '$timeout', 'psAuthorization'];

  /* @ngInject */
  function hasPermission($log, $timeout, psAuthorization) {
    var directive = {
      restrict: 'EA',
      link: link,
      replace: true,
      scope: {}
    };

    return directive;

    function link(scope, element, attrs) {
      psAuthorization.hasPermission().then(function (data) {

      }, function (error) {

      })
    }
  }

})();
