/**
 * Created by Misikir on 2/15/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.storage')
    .factory('psStorage', psStorage);

    psStorage.$inject = ['$q', '$localForage'];

  /* @ngInject */
  function psStorage($q, $localForage) {
    var service = {
      set: set,
      get: get,
      setObject: setObject,
      getObject: getObject,
      removeItem: removeItem
    };
    return service;

    ////////////////

    function set(key, value) {
      var deferred = $q.defer();
      $localForage.setItem(key, value).then(
        function (data) {
          deferred.resolve(data);
        });
      return deferred.promise;
    }

    function get(key) {
      var deferred = $q.defer();
      $localForage.getItem(key).then(
        function (data) {
          deferred.resolve(data);
        });
      return deferred.promise;
    }

    function setObject(key, value) {
      var deferred = $q.defer();
      $localForage.setItem(key, JSON.stringify(value)).then(
        function (data) {
          deferred.resolve(data);
        });
      return deferred.promise;
    }

    function getObject(key) {
      var deferred = $q.defer();
      $localForage.getItem(key).then(
        function (data) {
          if (data) {
            deferred.resolve(data);
          } else {
            return deferred.reject(data);
          }

        });
      return deferred.promise;
    }

    function removeItem(key) {
      var deferred = $q.defer();
      $localForage.removeItem(key).then(
        function (data) {
          deferred.resolve(data);
        });
      return deferred.promise;
    }
  }

})();

