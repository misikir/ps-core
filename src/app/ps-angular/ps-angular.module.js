/**
 * Created by misikirA on 4/15/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.angular', [
      'ui.router',
      'ui.bootstrap',
      'ngSanitize',
      'ghiscoding.validation',
      'pascalprecht.translate',
      'gettext',
      'LocalForageModule',
      'ngAnimate',
      'summernote',
      'ngFileUpload',
      'angularPromiseButtons'
    ])
})();
