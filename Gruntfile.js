/**
 * Created by Misikir on 9/2/2016.
 */
'use strict';

module.exports = function (grunt) {

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Automatically load required Grunt tasks
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin',
    ngtemplates: 'grunt-angular-templates',
    nggettext_extract: 'grunt-angular-gettext',
    nggettext_compile: 'grunt-angular-gettext',
    cdnify: 'grunt-google-cdn'
  });

  // Configurable paths for the application
  var appConfig = {
    appName: 'ps-core',
    devPath: require('./bower.json').appPath || 'src',
    prodPath: 'dist',
    bowerPath: 'bower_components',
    demoPath: 'demo',
    env: ''
  };


  grunt.initConfig({

    // Project settings
    config: appConfig,

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= config.prodPath %>/**/*',
            '!<%= config.prodPath %>/.git/**/*'
          ]
        }]
      }

    },
    copy: {
      dist: {
        files: [
          {
            expand: true,
            cwd: '<%= config.bowerPath %>/angular-promise-buttons/src/styles/',
            src: ['**'],
            dest: '<%= config.devPath %>/assets/scss/angular-promise-buttons/'
          }, {
            expand: true,
            cwd: '<%= config.bowerPath %>/summernote/dist/font/',
            src: ['**'],
            dest: '<%= config.devPath %>/assets/fonts/'
          },
          {
            expand: true,
            cwd: '<%= config.bowerPath %>/summernote/dist/font/',
            src: ['**'],
            dest: '<%= config.prodPath %>/fonts/'
          },

          {
            cwd: '<%= config.bowerPath %>/angular-bootstrap/',
            src: 'ui-bootstrap-csp.css',
            dest: '<%= config.devPath %>/assets/scss/angular-bootstrap/',
            expand: true,
            rename: function (dest, src) {
              return dest + src.replace(/\.css$/, ".scss");
            }
          },
          {
            expand: true,
            cwd: '<%= config.devPath %>/assets/scss/',
            src: ['**'],
            dest: '<%= config.prodPath %>/scss/'

          },
          {
            expand: true,
            cwd: '<%= config.devPath %>/assets/fonts/',
            src: ['**'],
            dest: '<%= config.prodPath %>/fonts/'
          },

          {
            expand: true,
            cwd: '<%= config.devPath %>/assets/img/',
            src: ['**'],
            dest: '<%= config.prodPath %>/img/'

          }

        ]
      }
    },
    concat: {
      options: {
        // define a string to put between each file in the concatenated output
        //separator: ';'
      },
      core: {
        src: [
          '<%= config.devPath %>/app/ps-core/modules/**/*.module.js',
          '<%= config.devPath %>/app/ps-core/modules/**/*.message.js',
          '<%= config.devPath %>/app/ps-core/modules/**/*.controller.js',
          '<%= config.devPath %>/app/ps-core/modules/**/*.directive.js',
          '<%= config.devPath %>/app/ps-core/modules/**/*.factory.js',
          '<%= config.devPath %>/app/ps-core/modules/**/*.service.js',
          '<%= config.devPath %>/app/ps-core/modules/**/*.filter.js',
          '<%= config.devPath %>/app/ps-core/modules/**/*.route.js',
          '<%= config.devPath %>/app/ps-core/modules/**/*.config.js',
          '.tmp/templateCache.js',
          '<%= config.devPath %>/app/ps-core/ps-core.module.js',
          '<%= config.devPath %>/app/ps-core/ps-core.config.js'
        ],
        dest: '<%= config.prodPath %>/js/ps-core.js'
      },
      angular: {
        src: [
          '<%= config.bowerPath %>/lodash/dist/lodash.min.js',
          '<%= config.bowerPath %>/jquery/dist/jquery.min.js',
          '<%= config.bowerPath %>/angular/angular.min.js',
          '<%= config.bowerPath %>/bootstrap-sass/assets/javascripts/bootstrap.min.js',
          '<%= config.bowerPath %>/angular-bootstrap/ui-bootstrap-tpls.min.js',
          '<%= config.bowerPath %>/angular-ui-router/release/angular-ui-router.min.js',
          '<%= config.bowerPath %>/angular-gettext/dist/angular-gettext.min.js',
          '<%= config.bowerPath %>/angular-translate/angular-translate.min.js',
          '<%= config.bowerPath %>/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js',
          '<%= config.bowerPath %>/angular-animate/angular-animate.min.js',
          '<%= config.bowerPath %>/angular-sanitize/angular-sanitize.min.js',
          '<%= config.bowerPath %>/angular-promise-buttons/dist/angular-promise-buttons.min.js',
          '<%= config.bowerPath %>/summernote/dist/summernote.min.js',
          '<%= config.bowerPath %>/angular-summernote/dist/angular-summernote.min.js',
          '<%= config.bowerPath %>/localforage/dist/localforage.min.js',
          '<%= config.bowerPath %>/angular-localforage/dist/angular-localForage.min.js',
          '<%= config.bowerPath %>/icheck/icheck.min.js',
          '<%= config.bowerPath %>/lobibox/dist/js/lobibox.min.js',
          '<%= config.bowerPath %>/angular-validation-ghiscoding/dist/angular-validation.min.js',
          '<%= config.bowerPath %>/ng-file-upload/ng-file-upload-all.min.js',
          '<%= config.devPath %>/app/ps-angular/ps-angular.module.js'
        ],
        dest: '<%= config.prodPath %>/js/ps-angular.js'
      }
    },
    uglify: {
      options: {
        //compress: true,
        mangle: true,
        sourceMap: true
      },
      dist: {
        files: {
          '<%= config.prodPath %>/js/ps-angular.min.js': ['<%= config.prodPath %>/js/ps-angular.js'],
          '<%= config.prodPath %>/js/ps-core.min.js': ['<%= config.prodPath %>/js/ps-core.js']
        }
      }
    },
    sass: {                              // Task
      dist: {                            // Target
        options: {                       // Target options
          style: 'expanded'
        },
        files: {
          '<%= config.prodPath %>/css/ps-angular.css': '<%= config.devPath %>/assets/scss/ps-angular.scss',
          '<%= config.prodPath %>/css/ps-core.css': '<%= config.devPath %>/assets/scss/ps-core.scss'
        }
      }
    },
    cssmin: {
      options: {
        mergeIntoShorthands: false,
        roundingPrecision: -1
      }
      ,
      target: {
        files: [
          {
            '<%= config.prodPath %>/css/ps-angular.min.css': [
              '<%= config.prodPath %>/css/ps-angular.css']
          },
          {
            '<%= config.prodPath %>/css/ps-core.min.css': '<%= config.prodPath %>/css/ps-core.css'
          }
        ]

      },
    },
    // Automatically inject Bower components into the app
    wiredep: {
      app: {
        src: ['<%= config.demoPath %>/index.html'],
        ignorePath: /\.\.\//
      }
    },
    injector: {
      options: {
        relative: true,
        addRootSlash: false
      }
      ,
      dev: {
        files: {
          '<%= config.demoPath %>/index.html': [
            '<%= config.demoPath %>/env.dev.js',
            '<%= config.demoPath %>/**/*.module.js',
            '<%= config.demoPath %>/**/*.message.js',
            '<%= config.demoPath %>/**/*.controller.js',
            '<%= config.demoPath %>/**/*.directive.js',
            '<%= config.demoPath %>/**/*.factory.js',
            '<%= config.demoPath %>/**/*.service.js',
            '<%= config.demoPath %>/**/*.filter.js',
            '<%= config.demoPath %>/**/*.route.js',
            '<%= config.demoPath %>/**/*.config.js'

          ]
        }

      }

    },
    htmlmin: {
      dist: {
        options: {
          collapseWhitespace: true,
          conservativeCollapse: true,
          collapseBooleanAttributes: true,
          removeCommentsFromCDATA: true
        },
        files: [{
          expand: true,
          cwd: '<%= config.devPath %>',
          src: ['*.html'],
          dest: '<%= config.prodPath %>'
        }]
      }
    },
    ngtemplates: {
      dist: {
        options: {
          module: 'ps.core.views',
          htmlmin: '<%= htmlmin.dist.options %>',
          usemin: 'assets/js/app.js'
        },
        cwd: '<%= config.devPath %>',
        src: 'app/ps-core/modules/**/*.html',
        dest: '.tmp/templateCache.js'
      }
    }

  })
  ;


  grunt.registerTask('default', ['watch']);

  grunt.registerTask('dist', [
    'clean',
    'ngtemplates',
    'concat',
    'uglify',
    'copy',
    'sass',
    'cssmin'

  ]);
  grunt.registerTask('inject_demo', [
    'wiredep',
    'injector:dev'
  ]);


};
