/**
 * Created by Misikir on 4/25/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.account', [])
})();

/**
 * Created by misikirA on 4/14/2017.
 */
(function () {
    'use strict';

    angular
        .module('ps.core.api', [])
})();
/**
 * Created by misikirA on 4/14/2017.
 */
(function () {
    'use strict';

    angular
        .module('ps.core.collection', [])
})();
/**
 * Created by misikirA on 4/14/2017.
 */
(function () {
    'use strict';

    angular
        .module('ps.core.messaging', [])
})();

/**
 * Created by misikirA on 4/14/2017.
 */
(function () {
    'use strict';

    angular
        .module('ps.core.nav-bar', [])
})();

/**
 * Created by misikirA on 4/14/2017.
 */
(function () {
    'use strict';

    angular
        .module('ps.core.page-title', [])
})();
/**
 * Created by misikirA on 4/14/2017.
 */
(function () {
    'use strict';

    angular
        .module('ps.core.storage', [])
})();
/**
 * Created by Misikir on 3/28/2017.
 */

(function () {
  'use strict';

  angular
    .module('ps.core.translation', [])
})();

/**
 * Created by misikirA on 4/14/2017.
 */
(function () {
    'use strict';

    angular
        .module('ps.core.utility', [])
})();

/**
 * Created by Misikir on 4/25/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.views', [])
})();

/**
 * Created by misikirA on 4/14/2017.
 */
(function () {
    'use strict';

    angular
        .module('ps.core.wizard', [])
})();

/**
 * Created by Misikir on 3/28/2017.
 */
(function () {
  'use strict';
  /***
   * Alert and Notification type
   * 'error E |success S |warning W |information I'
   *
   */
  angular
    .module('ps.core.translation')
    .constant('translationMessage',
      {
        /*------------ Error -------------------*/
        E0001: {
          type: "error",
          message: ""
        },
        /*------------ Success -------------------*/
        S0001: {
          type: "success",
          message: ""
        },
        /*------------ Warning -------------------*/
        W0001: {
          type: "warning",
          message: ""
        },
        /*------------ Info -------------------*/
        I0001: {
          type: "information",
          message: ""
        }

      }
    );
})();

/**
 * Created by misikirA on 4/11/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.translation')
    .controller('LanguageSelectorTranslationController', LanguageSelectorTranslationController);

  LanguageSelectorTranslationController.$inject = ['$log', 'gettextCatalog', 'psTranslation'];

  /* @ngInject */
  function LanguageSelectorTranslationController($log, gettextCatalog, psTranslation) {
    var vm = this;
    vm.title = 'LanguageSelectorTranslationController';
    vm.selectedLanguage;
    vm.changeLanguage = changeLanguage;
    activate();

    ////////////////

    function activate() {
      $log.log(vm.title);

      vm.languages = psTranslation.getLanguages();
      vm.selectedLanguage = vm.languages[0].name;


    }

    function changeLanguage(selectedLanguage) {
      vm.selectedLanguage = selectedLanguage.name;

      psTranslation.getTranslation(selectedLanguage.code).then(
        function (data) {
          $log.log(data);
        });


      psTranslation.setCurrentLanguage(selectedLanguage).then(
        function (data) {
          var selectedLanguage = JSON.parse(data);
          gettextCatalog.setCurrentLanguage(selectedLanguage.code);
          $log.log('selectedLanguage', selectedLanguage);
        }
      );
      $log.log(gettextCatalog.getCurrentLanguage());
    }
  }

})();


/**
 * Created by Misikir on 3/27/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.translation')
    .controller('ModalTranslationTranslationController', ModalTranslationTranslationController);

  ModalTranslationTranslationController.$inject = ['$log', '$uibModalInstance', 'translationService', 'messaging', 'translationMessage', 'toTranslate', 'inputType', 'moduleType'];

  /* @ngInject */
  function ModalTranslationTranslationController($log, $uibModalInstance, translationService, messaging, translationMessage, toTranslate, inputType, moduleType) {
    var vm = this;
    vm.title = 'ModalTranslationTranslationController';
    vm.toTranslate = toTranslate;
    vm.inputType = inputType;
    vm.moduleType = moduleType;
    vm.done = done;
    vm.cancel = cancel;
    activate();

    ////////////////

    function activate() {
      $log.log(vm.title);
      vm.languages = translationService.getActiveLanguages();

      vm.translation = {
        Key: vm.toTranslate,
        Module: vm.moduleType,
        Languages: []
      };


      _.map(vm.languages, function (o) {
        var language = {
          Code: o.code,
          Name: o.name,
          Value: {
            Translated: ''
          }
        };
        vm.translation.Languages.push(language)
      })


    }

    function done() {

      translationService.saveTranslationByModule(vm.translation).then(
        function (data) {
          showMessaging(data);
          $uibModalInstance.close(vm.translation);
        }, function (error) {
          $log.log('error', error);
          messaging.notification(translationMessage.I0001);
        });

    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');

    }

    function showMessaging(data) {
      if (!data.IsFailed) {
        messaging.notification(messaging.formatMessage(data.SystemMessage));

      } else {
        messaging.dialog(messaging.formatMessage(data.SystemMessage));
      }


      $log.log('data.SystemMessage:', messaging.formatMessage(data.SystemMessage));
    }
  }

})();


/**
 * Created by Misikir on 4/25/2017.
 */
(function () {
  'use strict';

  /**
   * header title
   *
   * usage:
   * <has-permission  name="CCMS" default="How can we help you?" ></has-permission>
   *
   */

  angular.module('ps.core.account')
    .directive('hasPermission', hasPermission);

  hasPermission.$inject = ['$log', '$timeout', 'psAuthorization'];

  /* @ngInject */
  function hasPermission($log, $timeout, psAuthorization) {
    var directive = {
      restrict: 'EA',
      link: link,
      replace: true,
      scope: {}
    };

    return directive;

    function link(scope, element, attrs) {
      psAuthorization.hasPermission().then(function (data) {

      }, function (error) {

      })
    }
  }

})();

/**
 * Created by misikirA on 2/1/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.collection')
    .directive('collection', collection);

  collection.$inject = ['$log'];

  /* @ngInject */
  function collection($log) {
    var directive = {
      transclude: true,
      scope: {
        colHeader: '@',
        colHeight: '@',
        colInput: '=',
        colInputTitle: '@',
        colInputActive: '@',
        colInputTemplate: '@',
        colOutput: '=',
        colOutputTitle: '@',
        colOutputActive: '@',
        colOutputTemplate: '@'
      },
      replace: true,
      restrict: 'EA',
      templateUrl: 'app/ps-core/modules/collection/views/collection.html',
      controller: ['$scope', '$element', function CollectionController($scope, $element) {

        $scope.inputData = $scope.colInput;
        $scope.outputData = $scope.colOutput;
        $scope.header = $scope.colHeader;
        $scope.count = 0;
        $scope.add = add;
        $scope.detail = detail;
        $scope.remove = removeCol;
        $scope.getOutput = getOutput;
        var val;

        activate();

        function activate() {

          if ($scope.colHeight === undefined) {
            $scope.colHeight = 400;
          }


          if ($scope.colInputActive === undefined) {

            $scope.active = true;
          } else {
            val = ($scope.colInputActive === "true");

            $scope.inputActive = val;
          }
          if ($scope.colOutputActive === undefined) {

            $scope.active = true;
          } else {
            val = ($scope.colOutputActive === "true");

            $scope.outputActive = val;
          }


          $element.css('height', $scope.colHeight + 'px');
          $element.find('.clctn-lft').css('height', $scope.colHeight + 'px');
          $element.find('.clctn-rght').css('height', $scope.colHeight + 'px');


          $scope.inputData = _.map($scope.inputData, function (element) {
            var output = _.find($scope.outputData, element);

            if (output) {
              var status = {npColSelected: true, npColActive: false}
            } else {
              var status = {npColSelected: false, npColActive: false}
            }
            return _.extend({}, element, status);
          });

          if ($scope.header === undefined) {
            $scope.header = 'name';
          }

        }

        function add(col) {
          col.npColSelected = true;
          getOutput();
          count();
        }

        function detail(col) {
          col.npColActive = !col.npColActive;

        }

        function removeCol(col) {
          col.npColSelected = false;
          getOutput();
          count();
        }


        function getOutput() {
          $scope.colOutput = _.filter($scope.inputData, {npColSelected: true});
        }

        function count() {
          $scope.count = _.filter($scope.inputData, {npColSelected: true}).length;
        }


      }]
    };
    return directive;
  }

})();


/**
 * Created by Misikir on 2/4/2017.
 */
/**
 * Created by Misikir on 10/3/2016.
 */
(function () {
  'use strict';

  /**
   * header title
   *
   * usage:
   * <page-title  name="CCMS" default="How can we help you?" ></page-title>
   *
   */

  angular.module('ps.core.page-title')
    .directive('pageTitle', pageTitle);

  pageTitle.$inject = ['$rootScope', '$timeout'];

  /* @ngInject */
  function pageTitle($rootScope, $timeout) {
    var directive = {
      restrict: 'EA',
      link: link,
      replace:true,
      scope: {
        name: "@",
        default: "@"
      }
    };

    return directive;

    function link(scope, element, attrs) {
      var title = scope.name + ' | ' + scope.default;
      element.replaceWith('<title>'+title+'</title>');

      var listener = function (event, toState, toParams, fromState, fromParams) {

        // Create your own title pattern
        if (toState.data && toState.data.pageTitle) title = scope.name + ' | ' + toState.data.pageTitle;
        $timeout(function () {
          element.replaceWith('<title>'+title+'</title>');

        });
      };
      $rootScope.$on('$stateChangeStart', listener);
    }
  }

})();


/**
 * Created by misikirA on 3/27/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.translation')
    .directive('psTranslate', psTranslate);

  psTranslate.$inject = ['$log', '$compile', '$timeout', '$uibModal'];

  /* @ngInject */
  function psTranslate($log, $compile, $timeout, $uibModal) {
    var directive = {
      link: link,
      restrict: 'A',
      //template: '',
      require: 'ngModel',
      scope: {
        ngModel: "=",
        psTranslateModule: "@",
        psTranslateInput: "@"
      }
    };
    return directive;

    function link(scope, element, attrs) {


      if (scope.psTranslateModule == undefined) {
        scope.psTranslateModule = "dictionary"
      }

      if ((scope.psTranslateInput == undefined) || (scope.psTranslateInput == "input")) {
        scope.inputType = "input"
      }
      else if (scope.psTranslateInput == "textarea") {
        scope.inputType = "textarea"
      }


      scope.openTranslator = function () {
        var modalInstance = $uibModal.open({
          size: 'md',
          templateUrl: 'app/ps-core/modules/collection/views/collection.html',
          controller: 'ModalTranslationTranslationController',
          controllerAs: 'translationCtrl',
          windowClass: 'validation-modal',
          windowTopClass: 'validation-modal',
          resolve: {
            toTranslate: function () {
              return scope.ngModel;
            },
            inputType: function () {
              return scope.inputType;
            },
            moduleType: function () {
              return scope.psTranslateModule;
            }
          }
        });

        modalInstance.result.then(
          function (selectedFormAttribute) {
            $log.log(selectedFormAttribute)
          }, function () {
            $log.log('Modal dismissed at: ' + new Date());
          });
      };

      scope.disable = false;
      if (scope.ngModel == undefined) {
        scope.disable = true;
      } else {
        scope.disable = false;
      }


      var translateBtn = '<div class="pstn-rltv">' +
        '<button class="btn  btn-default translate-btn" ' +
        'ng-disabled =" (ngModel == undefined) || (ngModel ==\'\' )" ' +
        'ng-click="openTranslator()">' +
        '<span class="badge bld">T</span>' +
        '</button>' +
        '</div>';

      var cc = $compile(translateBtn)(scope);
      var wrapper = angular.element(cc);

      element.after(wrapper);
      wrapper.prepend(element);


      scope.$on("$destroy", function () {
        wrapper.after(element);
        wrapper.remove();
      });


    }
  }


})();


/**
 * Created by Billet on 12/29/2016.
 */

;(function () {
  'use strict';

  angular.module('ps.core.account')
    .provider('psAuthentication', psAuthentication);

  psAuthentication.$inject = [];

  function psAuthentication() {
    var _options = {
      endpoint: {}
    };

    /* jshint validthis:true */
    this.$get = psAuthenticationHelper;
    this.config = config;

    psAuthenticationHelper.$inject = ['$rootScope', '$log', '$q', '$http', '$timeout', '$state', '$localForage'];
    function config(options) {
      _options = angular.merge({}, _options, options);
    }

    /* @ngInject */
    function psAuthenticationHelper($rootScope, $log, $q, $http, $timeout, $state, $localForage) {
      var service = {
        login: login,
        logout: logout,
        isLoggedIn: isLoggedIn
      };
      return service;

      function logout() {
        var deferred = $q.defer();
        $localForage.removeItem(['access_token', 'psAuthentication', 'permission']).then(function (data) {
          console.log(data);
          deferred.resolve(data);
        });
        return deferred.promise;
      }

      function login(loginData) {
        var deferred = $q.defer();


        var data = "grant_type=password&username=" + loginData.username + "&password=" + loginData.password + "&client_id=099153c2625149bc8ecb3e85e03f0022";

        $http({
          method: 'POST',
          url: _options.endpoint.login,
          data: data,
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(
          function (d) {
            $localForage.setItem('access_token', d.data.access_token).then(
              function (lfData) {
                d.data.access_token = "NO Access Token";
                $localForage.setItem('psAuthentication', d.data).then(
                  function (d) {
                    deferred.resolve(d);
                  });
              });

          },
          function (err) {
            deferred.reject(err.data);
          });
        return deferred.promise;
      };

      function isLoggedIn() {
        var deferred = $q.defer();
        $localForage.getItem('access_token').then(
          function (response) {
            deferred.resolve(response);
          });
        return deferred.promise;
      }
    }


  }
})();

/**
 * Created by Misikir on 2/20/2017.
 */
;(function () {
  'use strict';

  angular.module('ps.core.account')
    .provider('psAuthorization', psAuthorization);

  psAuthorization.$inject = [];

  function psAuthorization() {
    var _options = {
      endpoint: {
        getAllRoles:'',
        getPermission: '',
        setPermissions: '',
        hasPermission: ''
      }
    };

    /* jshint validthis:true */
    this.$get = psAuthorizationHelper;
    this.config = config;

    psAuthorizationHelper.$inject = ['$rootScope', '$log', '$q', '$http', '$timeout', '$state', '__config', '$localForage'];
    function config(options) {
      _options = angular.merge({}, _options, options);
    }

    /* @ngInject */
    function psAuthorizationHelper($rootScope, $log, $q, $http, $timeout, $state, __config, $localForage) {
      var service = {
        getAllRoles:getAllRoles,
        getPermission: getPermission,
        setPermissions: setPermissions,
        hasPermission: hasPermission

      };
      return service;

      function getAllRoles() {

      }
      function getPermission() {

      }

      function setPermissions() {

      }

      function hasPermission() {

      }
    }


  }
})();

/**
 * Created by Misikir on 11/19/2016.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.api')
    .provider('api', api);

  api.$inject = [];

  function api() {
    var _options = {
      endpoint: {}
    };

    /* jshint validthis:true */
    this.$get = apiHelper;
    this.config = config;

    apiHelper.$inject = ['$rootScope', '$log', '$q', '$http', '$timeout', '$state', 'Upload', '__env', 'psStorage'];

    /////////////////////////////

    function config(options) {
      _options = angular.merge({}, _options, options);
    }

    /* @ngInject */
    function apiHelper($rootScope, $log, $q, $http, $timeout, $state, Upload, __env, psStorage) {


      var service = {
        get: get,
        post: save,
        put: update,
        patch: patch,
        delete: remove,
        upload: upload,
        login: login,
      };
      return service;

      ////////////////


      function get(url) {
        var deferred = $q.defer();
        var req = {
          method: 'GET',
          url: _options.endpoint.hasOwnProperty(url) ? _options.endpoint[url] : url
        };

        base(req).then(function (data) {
          deferred.resolve(data);
        });
        return deferred.promise;
      }

      function save(url, data) {
        var deferred = $q.defer();
        var req = {
          method: 'POST',
          url: _options.endpoint.hasOwnProperty(url) ? _options.endpoint[url] : url,
          data: data
        };

        base(req).then(function (data) {
          deferred.resolve(data);
        });
        return deferred.promise;
      }

      function update(url, data) {


      }

      function patch() {

      }

      function remove() {

      }

      function upload(url, data) {
        var deferred = $q.defer();
        var u = _options.api.hasOwnProperty(url) ? _options.api[url] : url;

        var req = {
          url: u,
          data: data,
          objectKey: '',
          arrayKey: ''
        };

        psStorage.getObject('access_token').then(function (auth) {
          if (auth) {
            req = angular.merge({}, req, {headers: {'Authorization': 'Bearer ' + auth}});
          }
          $log.log(req);

          Upload.upload(req).then(function (resp) {
            deferred.resolve(resp);
          }, function (resp) {
            deferred.reject(resp);
          });

        });


        return deferred.promise;
      }

      function login(url, data) {
        var deferred = $q.defer();

        $http({
          method: 'POST',
          url: _options.endpoint.hasOwnProperty(url) ? _options.endpoint[url] : url,
          data: data,
          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
          .then(
            function (response) {
              psStorage.setObject('access_token', response.data.access_token)
                .then(
                  function () {

                  });
              response.data.access_token = "NO Access Token";
              psStorage.setObject('authorizationData', response.data)
                .then(
                  function () {
                    deferred.resolve(response);
                  });
              deferred.resolve(response);
            },
            function (response) {

              deferred.reject(response);
            });
        return deferred.promise;
      };


      function base(req) {

        var deferred = $q.defer();


        psStorage.getObject('access_token')
          .then(
            function (auth) {
              if (auth) {
                req = angular.merge({}, req, {headers: {'Authorization': 'Bearer ' + auth}});
                $http(req).then(
                  function (data) {
                    deferred.resolve(data);
                  },
                  function (err) {
                    if (err.status === '401') {
                      $state.go('login');
                    }
                    return $q.reject(err)
                  });
              } else {
                $http(req).then(
                  function (data) {
                    deferred.resolve(data);
                  },
                  function (err) {
                    if (err.status === '401') {
                      $state.go('login');
                    }
                    return $q.reject(err)
                  });
              }
              // $log.log(req);


            },
            function (error) {
              $http(req).then(
                function (data) {
                  deferred.resolve(data);
                },
                function (err) {
                  if (err.status === '401') {
                    $state.go('login');
                  }
                  return $q.reject(err)
                });
              $log.log('access_token', error);
            });

        return deferred.promise;
      }
    }


  }
})();

/**
 * Created by Misikir on 3/8/2017.
 */
(function () {
  'use strict';
  // Check we have lobibox js included
  if (angular.isUndefined(window.Lobibox)) {
    throw "Please make sure lobibox is included! https://github.com/arboshiki/lobibox";
  }
  angular.module('ps.core.messaging')
    .provider('psMessaging', function ProvideMessaging() {

      return ({
        setDefaultMessaging: setDefaultMessaging,
        setDefaultNotify: setDefaultNotify,
        $get: ['$log', 'api', instantiateMessaging]
      });

      function setDefaultMessaging(options) {

        Lobibox.base.DEFAULTS = $.extend({}, Lobibox.base.DEFAULTS, options);
      }

      function setDefaultNotify(options) {
        Lobibox.notify.DEFAULTS = $.extend({}, Lobibox.notify.DEFAULTS, options);

      }

      function instantiateMessaging($log, api) {

        return {
          dialog: dialog,
          notification: notification,
          formatMessage: formatMessage,
          /*------ Default ------------*/
          confirm: confirm,
          alert: alert,
          prompt: prompt,
          progress: progress,
          window: window,
          notify: notify

        };

        function formatMessage(systemMessage) {
          var system = {
            "Exception": null,
            "ExceptionMessage": null,
            "Culture": null,
            "MessageType": 0,
            "Type": "Error",
            "Message": "Attribute Name Already Exist Within Your Organization",
            "MessageCode": "E400",
            "Code": "E400-",
            "HasDetail": false,
            "ModuleCode": null
          };


          var sysMsg = {
            type: systemMessage.Type.toLowerCase(),
            message: systemMessage.Message
          };
          if (systemMessage.HasDetail) {
            sysMsg.buttons = {
              detail: {
                'class': 'btn btn-info',
                'name': 'Detail',
                closeOnClick: false
              }
            };
            sysMsg.callback = function (lobibox, type) {
              if (type === 'detail') {
                $log.log('Call extra Detail', systemMessage)
              }

            };

          }

          return sysMsg;
        }

        function dialog(message) {


          //message.msg="modified msg";
          message.msg = message.message;

          if (message.type == "information") {
            message.type = "info";
            message.title = "Information";
          } else if (message.type == "success") {
            message.title = "Success";
          } else if (message.type == "error") {
            message.title = "Error";
          } else if (message.type == "warning") {
            message.title = "Warning";
          } else {
            message.title = message.type;

          }

          Lobibox.alert(message.type, message);
        }

        function notification(message) {

          if (message.type == "information") {
            message.type = "info";
            message.title = "Information";
          } else if (message.type == "success") {
            message.title = "Success";
          } else if (message.type == "error") {
            message.title = "Error";
            message.delay = false;
          } else if (message.type == "warning") {
            message.title = "Error";
            message.delay = false;
          } else {
            message.title = message.type;
            message.delay = false;
          }


          message.msg = message.message;
          // message.position = {position: 'top left'} //AVAILABLE OPTIONS: 'top left', 'top right', 'bottom left', 'bottom right'
          Lobibox.notify(message.type, message);

        }


        function getMessage(code) {
          api.get().then(function (data) {

          })

        }

        /*------ Default ------------

         horizontalOffset: 5,                //If the messagebox is larger (in width) than window's width. The messagebox's width is reduced to window width - 2 * horizontalOffset
         verticalOffset: 5,                  //If the messagebox is larger (in height) than window's height. The messagebox's height is reduced to window height - 2 * verticalOffset
         width: 600,
         height: 'auto',                     // Height is automatically calculated by width
         closeButton: true,                  // Show close button or not
         draggable: false,                   // Make messagebox draggable
         customBtnClass: 'lobibox-btn lobibox-btn-default', // Class for custom buttons
         modal: true,
         debug: false,
         buttonsAlign: 'center',             // Position where buttons should be aligned
         closeOnEsc: true,                   // Close messagebox on Esc press
         delayToRemove: 200,                 // Time after which lobibox will be removed after remove call. (This option is for hide animation to finish)
         delay: false,                       // Time to remove lobibox after shown
         baseClass: 'animated-super-fast',   // Base class to add all messageboxes
         showClass: 'zoomIn',                // Show animation class
         hideClass: 'zoomOut',               // Hide animation class
         iconSource: 'bootstrap',            // "bootstrap" or "fontAwesome" the library which will be used for icons
         */
        function confirm(values) {
          Lobibox.confirm(values);
        }

        function alert(type, options) {
          Lobibox.alert(type, options);
        }

        function prompt(type, options) {
          Lobibox.prompt(type, options);
        }

        function progress(options) {
          Lobibox.progress(options);
        }

        function window(options) {
          Lobibox.window(options);
        }

        /*
         Default options for lobibox notifications
         -----------------------------------------
         title: true,                // Title of notification. Do not include it for default title or set custom string. Set this false to disable title
         size: 'normal',             // normal, mini, large
         soundPath: 'src/sounds/',   // The folder path where sounds are located
         soundExt: '.ogg',           // Default extension for all sounds
         showClass: 'zoomIn',        // Show animation class.
         hideClass: 'zoomOut',       // Hide animation class.
         icon: true,                 // Icon of notification. Leave as is for default icon or set custom string
         msg: '',                    // Message of notification
         img: null,                  // Image source string
         closable: true,             // Make notifications closable
         delay: 5000,                // Hide notification after this time (in miliseconds)
         delayIndicator: true,       // Show timer indicator
         closeOnClick: true,         // Close notifications by clicking on them
         width: 400,                 // Width of notification box
         sound: true,                // Sound of notification. Set this false to disable sound. Leave as is for default sound or set custom soud path
         position: "bottom right"    // Place to show notification. Available options: "top left", "top right", "bottom left", "bottom right"
         iconSource: "bootstrap"     // "bootstrap" or "fontAwesome" the library which will be used for icons
         rounded: false,             // Whether to make notification corners rounded
         messageHeight: 60,          // Notification message maximum height. This is not for notification itself, this is for .lobibox-notify-msg
         pauseDelayOnHover: true,    // When you mouse over on notification, delay will be paused, only if continueDelayOnInactiveTab is false.
         onClickUrl: null,           // The url which will be opened when notification is clicked
         showAfterPrevious: false,   // Set this to true if you want notification not to be shown until previous notification is closed. This is useful for notification queues
         continueDelayOnInactiveTab: true, // Continue delay when browser tab is inactive
         */
        function notify(type, options) {
          Lobibox.notify(type, options);
        }


      }

    });
})();

/**
 * Created by Misikir on 2/15/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.storage')
    .factory('psStorage', psStorage);

    psStorage.$inject = ['$q', '$localForage'];

  /* @ngInject */
  function psStorage($q, $localForage) {
    var service = {
      set: set,
      get: get,
      setObject: setObject,
      getObject: getObject,
      removeItem: removeItem
    };
    return service;

    ////////////////

    function set(key, value) {
      var deferred = $q.defer();
      $localForage.setItem(key, value).then(
        function (data) {
          deferred.resolve(data);
        });
      return deferred.promise;
    }

    function get(key) {
      var deferred = $q.defer();
      $localForage.getItem(key).then(
        function (data) {
          deferred.resolve(data);
        });
      return deferred.promise;
    }

    function setObject(key, value) {
      var deferred = $q.defer();
      $localForage.setItem(key, JSON.stringify(value)).then(
        function (data) {
          deferred.resolve(data);
        });
      return deferred.promise;
    }

    function getObject(key) {
      var deferred = $q.defer();
      $localForage.getItem(key).then(
        function (data) {
          if (data) {
            deferred.resolve(data);
          } else {
            return deferred.reject(data);
          }

        });
      return deferred.promise;
    }

    function removeItem(key) {
      var deferred = $q.defer();
      $localForage.removeItem(key).then(
        function (data) {
          deferred.resolve(data);
        });
      return deferred.promise;
    }
  }

})();


/**
 * Created by Misikir on 3/28/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.translation')
    .provider('psTranslation', psTranslation);


  psTranslation.$inject = [];

  /* @ngInject */
  function psTranslation() {
    var _options = {
      endpoint: {
        getTranslation: '',
        getSuggestion: '',

        getTranslationByModule: '',
        getTranslationByKey: '',
        getTranslationByKeyByModule: '',

        saveTranslationByModule: ''
      }
    };

    /* jshint validthis:true */
    this.$get = translationHelper;
    this.config = config;

    translationHelper.$inject = ['$log', '$q', 'api', 'psStorage'];

    function config(options) {
      _options = angular.merge({}, _options, options);
    }

    /* @ngInject */
    function translationHelper($log, $q, api, psStorage) {

      var service = {
        getLanguages: getLanguages,
        getActiveLanguages: getActiveLanguages,
        getTranslation: getTranslation,
        getCurrentLanguage: getCurrentLanguage,
        setCurrentLanguage: setCurrentLanguage,
        getSuggestion: getSuggestion,
        saveTranslationByModule: saveTranslationByModule
      };
      return service;

      ////////////////

      function getLanguages() {
        return [
          {code: "en", name: "English"},
          {code: "am", name: "አማርኛ"},
          {code: "om", name: "Afaan Oromo"},
          {code: "ti", name: "ትግርኛ"}
        ]
      }

      function getActiveLanguages() {
        return [
          {code: "am", name: "አማርኛ"},
          {code: "om", name: "Afaan Oromo"},
          {code: "ti", name: "ትግርኛ"}
        ]
      }

      function getCurrentLanguage() {
        var deferred = $q.defer();
        psStorage.getObject('language').then(
          function (data) {
            deferred.resolve(JSON.parse(data));
          });
        return deferred.promise;
      }

      function setCurrentLanguage(data) {
        var deferred = $q.defer();
        psStorage.setObject('language', data).then(
          function (d) {
            deferred.resolve(d);
          });
        return deferred.promise;
      }

      function getSuggestion(data) {
        return base(_options.endpoint.getSuggestion, data);
      }

      function saveTranslationByModule(data) {
        return base(_options.endpoint.saveTranslationByModule, data);
      }

      function getTranslation(data) {
        var deferred = $q.defer();
        api.post(_options.endpoint.getTranslation, data).then(
          function (d) {
            psStorage.setObject('translation', d.data).then(
              function () {
                deferred.resolve(d.data);
              });

          },
          function (err) {
            $log.log(err);
            return $q.reject(err)
          });

        return deferred.promise;
      }

      /*--------   Base   -----------*/

      function base(url, data) {
        var deferred = $q.defer();
        api.post(url, data).then(
          function (d) {
            deferred.resolve(d.data);
          },
          function (err) {
            $log.log(err);
            return $q.reject(err)
          });

        return deferred.promise;
      }
    }
  }

})();


/**
 * Created by Misikir on 2/9/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.utility')
    .filter('cameCaseToHuman', cameCaseToHuman);

  function cameCaseToHuman() {
    return cameCaseToHumanFilter;

    ////////////////

    function cameCaseToHumanFilter(input, upperCaseFirst) {

      if (typeof input !== "string") {
        return input;
      }

      var result = input.replace(/([a-z])([A-Z])/g, '$1 $2')
        .replace(/([A-Z])([a-z])/g, ' $1$2')
        .replace(/\ +/g, ' ');

      if (upperCaseFirst) {
        result = result.charAt(0).toUpperCase() + result.slice(1);
      }

      return result;
    }
  }

})();


/**
 * Created by Misikir on 11/28/2016.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.utility')
    .filter('capitalize', capitalize);

  function capitalize() {
    return capitalizeFilter;

    ////////////////

    function capitalizeFilter(input, all) {
      if (!input) {
        return '';
      }
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      return (!!input) ? input.replace(reg, function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }) : '';
    }
  }

})();


/**
 * Created by Misikir on 11/29/2016.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.utility')
    .filter('randomColor', randomColor);

  function randomColor() {
    return randomColorFilter;

    ////////////////

    function randomColorFilter() {
      var clr = [
        '#FFFB00',
        '#84CF00',
        '#8C20BD',
        '#00B2DE',
        '#FF2000',
        '#FF7D00',
        '#0024BD',
        '#00BA00',
        '#FFCF00',
        '#F72494',
        '#0079C6',
        '#FFA600'];
      return clr[Math.floor(Math.random() * clr.length)];
    }
  }

})();


/**
 * Created by Misikir on 11/24/2016.
 */
(function () {
  'use strict';


  /*
   * Creates a range
   * Usage example: <option ng-repeat="y in [] | range:1998:1900">{{y}}</option>
   */


  angular
    .module('ps.core.utility')
    .filter('range', range);

  function range() {
    return rangeFilter;

    ////////////////

    function rangeFilter(input, start, end) {
      var direction;

      start = parseInt(start);
      end = parseInt(end);

      if (start === end) { return [start]; }

      direction = (start <= end) ? 1 : -1;

      while (start != end) {
        input.push(start);

        if (direction < 0 && start === end + 1) {
          input.push(end);
        }

        if (direction > 0 && start === end - 1) {
          input.push(end);
        }

        start += direction;
      }

      return input;
    }
  }

})();


/**
 * Created by misikirA on 3/24/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.utility')
    .filter('slug', slug);

  function slug() {
    return slugFilter;

    ////////////////

    function slugFilter(input) {
      if (!input) {
        return '';
      }
      return input.toLowerCase().replace(/ /g, "_");
    }
  }

})();

/**
 * Created by misikirA on 4/13/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.utility')
    .filter('stripTag', stripTag);

  function stripTag() {
    return stripTagFilter;

    ////////////////

    function stripTagFilter(input) {

     // input = "..." + input;
     // return input.replace(/(<([^>]+)>)/g, "");
      return  input ? String(input).replace(/<[^>]+>/gm, '') : '';
      //return angular.element(String(input)).text();

    }


  }

})();

/**
 * Created by Misikir on 2/14/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core.utility')
    .filter('trimTo', trimTo);

  function trimTo() {
    return trimToFilter;

    ////////////////

    function trimToFilter(input, limit, ellipsis) {
      if (!input) {
        return '';
      }
      var extra = " ...";
      if (ellipsis) {
        extra = ellipsis
      }

      var result = input.substr(0, limit);
      if (input.length > limit) {
        result = result + ' ' + extra;
      }
      return result
    }
  }

})();


/**
 * Created by Misikir on 3/28/2017.
 */

angular.module('ps.core.views').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('app/ps-core/modules/collection/views/collection.html',
    "<div class=\"col-xs-12 p-0 clctn-cnt bg-w\"> <div class=\"col-xs-6 clctn-lft\" ng-style=\"height\"> <div class=\"hdr-ttl bld\"> {{colInputTitle}} </div> <div class=\"form-group m-v-10\"> <div class=\"input-group\"> <input type=\"text\" class=\"form-control\" ng-model=\"inputSearch\" placeholder=\"Filter {{colInputTitle}}\"> <div class=\"input-group-addon\"> <i class=\"fa fa-search\"></i> </div> </div> </div> <div ng-repeat=\"item in inputData |  filter:{npColSelected:false, $:inputSearch}\"> <div class=\"col-xs-12 clctn-dtl\"> <div class=\"clctn-ttl\"> {{item[header]}} <span class=\"f-rght\"> <span ng-click=\"add(item)\" class=\"clctn-btn-sm\"><i class=\"fa fa-plus text-success\"></i></span> <span ng-click=\"detail(item)\" class=\"clctn-btn-sm\"> <i class=\"fa text-muted\" ng-class=\"item.npColActive ? 'fa-chevron-up':'fa-chevron-down'\"></i> </span> </span> </div> <div class=\"clctn-hdn\" ng-class=\"(item.npColActive || inputActive) ? 'actv':''\" ng-include=\"colInputTemplate\"></div> </div> </div> </div> <div class=\"col-xs-6 clctn-rght\" ng-style=\"height\"> <div class=\"hdr-ttl bld\"> {{colOutputTitle}} <span class=\"f-rght\"> <span class=\"bld\">{{count}}</span> item Selected </span> </div> <div class=\"form-group m-v-10\"> <div class=\"input-group\"> <input type=\"text\" class=\"form-control\" ng-model=\"outputSearch\" placeholder=\"Filter {{colOutputTitle}}\"> <div class=\"input-group-addon\"> <i class=\"fa fa-search\"></i> </div> </div> </div> <div ng-repeat=\"item in inputData | filter:{npColSelected:true, $:outputSearch}\"> <div class=\"col-xs-12 clctn-dtl\"> <div class=\"clctn-ttl\"> {{item[header]}} <span class=\"f-rght\"> <span ng-click=\"remove(item)\" class=\"clctn-btn-sm\"><i class=\"fa fa-minus text-danger\"></i></span> <span ng-click=\"detail(item)\" class=\"clctn-btn-sm\"> <i class=\"fa text-muted\" ng-class=\"item.npColActive ? 'fa-chevron-up':'fa-chevron-down'\"></i> </span> </span> </div> <div class=\"clctn-hdn\" ng-class=\"(item.npColActive || outputActive) ? 'actv':''\" ng-include=\"colOutputTemplate\"></div> </div> </div> </div> </div>"
  );


  $templateCache.put('app/ps-core/modules/translation/views/language-selector.html',
    "<div class=\"translation-module\" uib-dropdown ng-controller=\"LanguageSelectorTranslationController as languageSelector\"> <a class=\"translate-btn\" uib-dropdown-toggle> {{languageSelector.selectedLanguage}} <span class=\"caret\"></span> </a> <ul uib-dropdown-menu class=\"m-t-xs\"> <li ng-repeat=\"language in languageSelector.languages\"> <a ng-click=\"languageSelector.changeLanguage(language)\">{{language.name}}</a> </li> </ul> </div>"
  );


  $templateCache.put('app/ps-core/modules/translation/views/modal-translation.html',
    "<div class=\"form-builder\" style=\"z-index: 4080 !important\"> <div class=\"modal-header\"> <h4 class=\"modal-title\" translate>Translation</h4> </div> <div class=\"modal-body row p-10\"> <div class=\"col-xs-12\"> <div class=\"tabs-container\"> <uib-tabset> <uib-tab ng-repeat=\"language in translationCtrl.translation.Languages\"> <uib-tab-heading> {{language.Name}} <span ng-if=\"language.changed\" style=\"color:red\">*</span> </uib-tab-heading> <div class=\"panel-body\"> <div class=\"form-group\"> <label class=\"control-label\" translate> Translate</label> <p class=\"form-control-static\"> {{translationCtrl.translation.Key}}</p> </div> <div class=\"form-group\"> <label class=\"control-label\" translate> Translate to</label> <input ng-if=\"translationCtrl.inputType=='input'\" type=\"text\" class=\"form-control\" ng-change=\"language.changed=true\" ng-model=\"language.Value.Translated\"> <textarea ng-if=\"translationCtrl.inputType=='textarea'\" class=\"form-control\" rows=\"3\" ng-change=\"language.changed=true\" ng-model=\"language.Value.Translated\">\r" +
    "\n" +
    "        </textarea> </div> </div> </uib-tab> </uib-tabset> </div> </div> </div> <div class=\"modal-footer\"> <button type=\"button\" class=\"btn btn-white\" ng-click=\"translationCtrl.cancel()\" translate>Cancel</button> <button type=\"button\" class=\"btn btn-primary\" ng-click=\"translationCtrl.done()\" translate>Done</button> </div> </div>"
  );

}]);

/**
 * Created by misikirA on 4/14/2017.
 */
(function () {
  'use strict';

  angular
    .module('ps.core', [
      /*------------------*/
      /*---  Views   ---*/
      /*------------------*/
      'ps.core.views',

      /*------------------*/
      /*---  Modules   ---*/
      /*------------------*/
      'ps.core.translation',
      'ps.core.account',
      /*------------------*/
      /*--- Directive  ---*/
      /*------------------*/
      'ps.core.collection',
      'ps.core.wizard',
      'ps.core.nav-bar',
      'ps.core.page-title',
      /*------------------*/
      /*---- Service  ----*/
      /*------------------*/
      'ps.core.api',
      'ps.core.storage',
      'ps.core.messaging',


      /*------------------*/
      /*---- Filter   ----*/
      /*------------------*/
      'ps.core.utility'
    ])
})();

/**
 * Created by misikirA on 4/20/2017.
 */
(function () {
  'use strict';

  angular.module('ps.core')
    .run(['$rootScope', '$state', function ($rootScope, $state) {

      $rootScope.$on('$stateChangeStart', function (evt, to, params) {
        if (to.redirectTo) {
          evt.preventDefault();
          $state.go(to.redirectTo, params, {location: 'replace'})
        }
      });
    }]);

})();

(function () {
  'use strict';

  angular
    .module('ps.core')
    .run(run);
  run.$inject = ['$log', '$rootScope', '$state', '__env', 'gettextCatalog', 'psTranslation'];

  /* @ngInject */
  function run($log, $rootScope, $state, __env, gettextCatalog, psTranslation) {
    gettextCatalog.debug = __env.enableDebug;
    loadLanguage();

    $rootScope.$on('$stateChangeStart', function (evt, to, params) {
      loadLanguage()
    });

    function loadLanguage() {
      psTranslation.getCurrentLanguage().then(
        function (data) {
          $log.log('current Language:', data.code);
          gettextCatalog.setCurrentLanguage(data.code);

        }
      );
    }
  }

})();
